<?php
namespace Atomic\LaravelCore;

use Atomic\LaravelCore\BusinessObject;
use DB;
use Validator;
use ReflectionClass;
use Log;

abstract class DefaultBusinessTransaction implements BusinessObject {
	abstract protected function prepare( $dto, $originalDto );
	abstract protected function process( $dto, $originalDto );

	public function execute($dto){
		$originalDto = $dto;
		$result = [];
		
		try {
			DB::beginTransaction();
			
			$validator = Validator::make($dto, $this->rules());

			if ($validator->fails()) {
				throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors());
			}

			$modified_dto = $this->prepare($dto, $originalDto);
			if($modified_dto != null) $dto = $modified_dto;
			$result =  $this->process($dto, $originalDto);
						
			DB::commit();
		}catch(CoreException $ex){
			DB::rollback();
			throw $ex;
		}
		
		return $result;
	}

	public function rules() {
		return [];
	}

	public function errorBusinessValidation($errorList=[]) {
		throw new CoreException(ERROR_BUSINESS_VALIDATION, $errorList);
	}

	public function errorDataValidation($errorList=[]) {
		throw new CoreException(ERROR_DATA_VALIDATION, $errorList);
	}

	public function errorPrivateBusinessValidation($errorList=[]) {
		throw new CoreException(ERROR_BUSINESS_VALIDATION, $errorList, ERROR_TYPE_PRIVATE);
	}

	public function errorPrivateDataValidation($errorList=[]) {
		throw new CoreException(ERROR_DATA_VALIDATION, $errorList, ERROR_TYPE_PRIVATE);
	}
}