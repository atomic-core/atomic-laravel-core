<?php

namespace Atomic\LaravelCore\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\Finder;
use Illuminate\Filesystem\Filesystem;
use Intervention\Image\ImageManagerStatic;
use Validator;

class PackageServiceProvider extends ServiceProvider
{
    protected $commands = [
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        require __DIR__ . '/define.php';

        Validator::extend('imageable', function ($attribute, $value, $params, $validator) {
            try {
                ImageManagerStatic::make($value);
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }, 'File not Image!');

        $this->bootBindings();
        
    }

    /**
     * Bind some Interfaces and implementations.
     */
    protected function bootBindings() {
        
    }

    /**
     * Require composer's autoload file the packages.
     *
     * @return void
     **/
    protected function loadAutoloader($path)
    {
        $finder = new Finder;
        $files = new Filesystem;
 
        $autoloads = $finder->in($path)->files()->name('autoload.php')->depth('<= 3')->followLinks();
 
        foreach ($autoloads as $file)
        {
            Log::info("Autoload: " . $file->getRealPath());
            $files->requireOnce($file->getRealPath());
        }
    }
}
