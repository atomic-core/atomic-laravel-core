<?php

namespace Atomic\LaravelCore;

/**
 * Created by Reinaldi Mukti.
 * Date: 11/11/19
 * Time: 15:07
 */
interface Query {

    /**
     * @return mixed
     */
    public function toString();

}