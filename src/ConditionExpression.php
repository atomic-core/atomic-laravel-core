<?php

namespace Atomic\LaravelCore;

/**
 * Class ConditionExpression
 *
 * @author Reinaldi Mukti 2019-11-11
 * Untuk membantu mempermudah penulisan suatu kondisi pada suatu query
 */
class ConditionExpression
{

    public static function likeCaseSensitive($column, $value){
        return $column." LIKE '%".$value."%' ";
    }

    public static function likeCaseInsensitive($column, $value){
        return " UPPER(".$column.") ILIKE '%".strtoupper($value)."%' ";
    }

    public static function equalCaseSensitive($column, $value){
        return $column." = '".$value."'";
    }

    public static function equalCaseInsensitive($column, $value){
        return " UPPER(".$column.") = UPPER('".$value."') ";
    }

    public static function notEqualCaseSensitive($column, $value){
        return $column." != '".$value."'";
    }

    public static function notEqualCaseInsensitive($column, $value){
        return " UPPER(".$column.") != UPPER('".$value."') ";
    }

}