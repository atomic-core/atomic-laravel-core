<?php
/**
 * User: Reinaldi Mukti
 * Date: 11/11/19
 * Time: 15:30 PM
 */

namespace Atomic\LaravelCore;

use Ramsey\Uuid\Uuid;

class ProcessLogDetailHelper
{

    public static function addProcessLogDetail(
        $param_process_log_id, $param_parameter_key, $param_parameter_value,
        $param_user_id, $param_timestamp
    ){

        $process_log_id     = $param_process_log_id;
        $parameter_key      = $param_parameter_key;
        $parameter_value    = json_encode($param_parameter_value);
        $user_id            = $param_user_id;
        $timestamp          = $param_timestamp;

        $process_log_details = \DB::table('process_log_details')->insert(
            [
                'process_log_id'        => $process_log_id,
                'parameter_key'         => $parameter_key,
                'parameter_value'       => $parameter_value,
                'created_at'            => $timestamp,
                'created_user_id'       => $user_id,
                'updated_at'            => $timestamp,
                'updated_user_id'       => $user_id
            ]
        );

        return $process_log_details;
    }

}