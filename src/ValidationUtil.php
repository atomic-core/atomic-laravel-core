<?php

namespace Atomic\LaravelCore;

use Validator;

class ValidationUtil {
    
    public static function valIsNull ($item){

        if(empty($item) || is_null($item)){
            return true;
        }
        return false;

    }
 
    public static function valPublicDataValidation($dto, $rules){

        $validator = Validator::make($dto, $rules);

        if ($validator->fails()) {
            throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors(), ERROR_TYPE_PUBLIC);
        }

        return true;
    }

    public static function valPublicDataValidationCustMessage($dto, $rules, $message){

        $validator = Validator::make($dto, $rules, $message);

        if ($validator->fails()) {
            throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors(), ERROR_TYPE_PUBLIC);
        }

        return true;
    }

    public static function valPrivateDataValidation($dto, $rules){

        $validator = Validator::make($dto, $rules);

        if ($validator->fails()) {
            throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors(), ERROR_TYPE_PRIVATE);
        }

        return true;
    }

    public static function valPrivateDataValidationCustMessage($dto, $rules, $message){

        $validator = Validator::make($dto, $rules, $message);

        if ($validator->fails()) {
            throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors(), ERROR_TYPE_PUBLIC);
        }

        return true;
    }
    
    public static function valKeyExists($dto, $key){

        if (array_key_exists($key, $dto)){
            return true;
        }else{
            return false;
        }
    }
}