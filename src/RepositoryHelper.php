<?php
/**
 * User: Reinaldi Mukti
 * Date: 11/11/19
 * Time: 15:30 PM
 */

namespace Atomic\LaravelCore;

class RepositoryHelper
{
    public static function auditActive($object, $datetime){
        $object->{'is_active'} 		= _YES;
        $object->{'active_at'} 		= $datetime;
        $object->{'nonactive_at'} 	= _NULL;
	}
	
	public static function auditNonActive($object, $datetime){
        $object->{'is_active'} 		= _NO;
        $object->{'nonactive_at'} 	= $datetime;
	}
	
	public static function auditInsert($object, $userLoginId, $datetime){
		$object->{'created_at'}			=  $datetime;
		$object->{'updated_at'}			=  $datetime;
		$object->{'created_user_id'}	=  $userLoginId;
		$object->{'updated_user_id'}	=  $userLoginId;
	}

	public static function auditUpdate($object, $userLoginId, $datetime){
		$object->{'updated_at'}			=  $datetime;
		$object->{'updated_user_id'}	=  $userLoginId;
    }
}