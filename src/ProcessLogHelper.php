<?php
/**
 * User: Reinaldi Mukti
 * Date: 11/11/19
 * Time: 15:30 PM
 */

namespace Atomic\LaravelCore;

use Ramsey\Uuid\Uuid;

class ProcessLogHelper
{

    public static function addProcessLog(
        $param_process_name, $param_user_id, $param_timestamp
    ){
        
        $uuid               = Uuid::uuid1();

        $process_name       = $param_process_name;
        $user_id            = $param_user_id;
        $timestamp          = $param_timestamp;

        $process_log_id = \DB::table('process_logs')->insertGetId(
            [
                'process_name'          => $process_name,
                'process_no'            => $uuid,
                'process_status'        => STATUS_DRAFT,
                'created_at'            => $timestamp,
                'created_user_id'       => $user_id,
                'updated_at'            => $timestamp,
                'updated_user_id'       => $user_id
            ]
        );

        return [
            "process_log_id"    => $process_log_id,
            "uuid"              => $uuid
        ];
    }

    public static function updateInProgressProcessLogStatus(
        $process_log_id, $user_id, $timestamp
    ){

        $process_logs = \DB::table('process_logs')
                    ->where('id', $process_log_id)
                    ->update(
                        [
                            'process_status'    => STATUS_IN_PROGRESS,
                            'updated_user_id'   => $user_id,
                            'updated_at'        => $timestamp
                        ]
                    );

        return $process_logs;
    }

    public static function updateSuccessProcessLogStatus(
        $process_log_id, $user_id, $timestamp
    ){

        $process_logs = \DB::table('process_logs')
                    ->where('id', $process_log_id)
                    ->update(
                        [
                            'process_status'    => STATUS_SUCCESS,
                            'updated_user_id'   => $user_id,
                            'updated_at'        => $timestamp
                        ]
                    );

        return $process_logs;
    }

    public static function updateErrorProcessLogStatus(
        $process_log_id, $user_id, $timestamp
    ){

        $process_logs = \DB::table('process_logs')
                    ->where('id', $process_log_id)
                    ->update(
                        [
                            'process_status'    => STATUS_ERROR,
                            'updated_user_id'   => $user_id,
                            'updated_at'        => $timestamp
                        ]
                    );

        return $process_logs;
    }

}