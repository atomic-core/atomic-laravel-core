<?php
/**
 * User: Reinaldi Mukti
 * Date: 11/11/19
 * Time: 15:30 PM
 */

namespace Atomic\LaravelCore;

class ResponseHelper
{

    public static function isSuccess($message, $data){

        $status = _RESPONSE_SUCCESS;
        $status_type = ERROR_TYPE_DEFAULT;
        $status_code = SUCCESS_CODE;
    
        return response()->json([
            "status"        => $status,
            "status_type"   => $status_type,
            "status_code"   => $status_code,
            "message"       => $message,
            "data"          => $data
        ],SUCCESS_CODE);

    }
    
    public static function isFail($ex){

        if($ex instanceof CoreException) {
            $data = '';
            $error_key = $ex->getErrorKey();
            $error_type = $ex->getErrorType();

            if($error_key == ERROR_DATA_VALIDATION) {

                $errorData = $ex->getErrorMessage();
                $converterJson = json_encode($errorData);
                $converterDecode = (array)json_decode($converterJson);
                $keys = array_keys($converterDecode);

                $errors = [];
                foreach ($keys as $value) {
                    $errors[$value] = $ex->getErrorMessage()->first($value);
                }

                $message = $errors;
                $status_code = ERROR_DATA_VALIDATION_CODE;

            } else if($error_key == ERROR_BUSINESS_VALIDATION) {
                $message = $ex->getErrorMessage();
                $status_code = ERROR_BUSINESS_VALIDATION_CODE;
            } else {
                $message = $ex->getErrorMessage();
                $status_code = UNDEFINED_ERROR_CODE;
            }

        } else {
            \Log::debug(get_class($ex));
        }
        
        return response()->json([
            "status"        => $error_key,
            "status_code"   => $status_code,
            "status_type"   => $error_type,
            "message"       => $message,
            "data"          => $data
        ],UNDEFINED_ERROR_CODE);
    }

}