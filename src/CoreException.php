<?php

namespace Atomic\LaravelCore;
use Exception;

class CoreException extends Exception {

    protected $errorKey;
    protected $errorMessage;
    protected $errorType;

    public function __construct() {
        $get_arguments       = func_get_args();
        $number_of_arguments = func_num_args();

        if (method_exists($this, $method_name = '__construct'.$number_of_arguments)) {
            call_user_func_array(array($this, $method_name), $get_arguments);
        }
    }

    public function __construct1($errorMessage = "") {
        $this->errorKey = ERROR_KEY_DEFAULT;
        $this->errorMessage = $errorMessage;
        $this->errorType = ERROR_TYPE_DEFAULT;
    }

    public function __construct2($errorKey, $errorMessage = "") {
        $this->errorKey = $errorKey;
        $this->errorMessage = $errorMessage;
        $this->errorType = ERROR_TYPE_DEFAULT;
    }

    public function __construct3($errorKey, $errorMessage = "", $errorType) {
        $this->errorKey = $errorKey;
        $this->errorMessage = $errorMessage;
        $this->errorType = $errorType;
    }

    public function getErrorKey() {
        return $this->errorKey;
    }

    public function getErrorMessage() {
        return $this->errorMessage;
    }

    public function getErrorType() {
        return $this->errorType;
    }

}