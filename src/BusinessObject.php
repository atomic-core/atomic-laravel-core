<?php 

namespace Atomic\LaravelCore;

/**
 * @author Reinaldi Mukti 
 */
interface BusinessObject {
	
	public function getDescription();
	public function execute( $dto );
	public function rules();
	public function errorBusinessValidation( $errorList=[] );
	
}