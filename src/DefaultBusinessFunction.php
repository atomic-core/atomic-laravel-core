<?php

namespace Atomic\LaravelCore;

use Atomic\LaravelCore\BusinessObject;
use Validator;

/**
 * Reinaldi Mukti, 11/11/2019
 * Default Business Function
 * 
 **/

abstract class DefaultBusinessFunction implements BusinessObject {
	abstract protected function process( $dto );

	public function execute($dto){

		$validator = Validator::make($dto, $this->rules());

		if ($validator->fails()) {
			throw new CoreException(ERROR_DATA_VALIDATION, $validator->errors());
		}

		return $this->process($dto);
		
	}

	public function rules() {
		return [];
	}

	public function errorBusinessValidation($errorList=[]) {
		throw new CoreException(ERROR_BUSINESS_VALIDATION, $errorList);
	}

	public function errorDataValidation($errorList=[]) {
		throw new CoreException(ERROR_DATA_VALIDATION, $errorList);
	}

	public function errorPrivateBusinessValidation($errorList=[]) {
		throw new CoreException(ERROR_BUSINESS_VALIDATION, $errorList, ERROR_TYPE_PRIVATE);
	}

	public function errorPrivateDataValidation($errorList=[]) {
		throw new CoreException(ERROR_DATA_VALIDATION, $errorList, ERROR_TYPE_PRIVATE);
	}
}