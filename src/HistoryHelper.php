<?php
/**
 * User: Reinaldi Mukti
 * Date: 11/11/19
 * Time: 15:30 PM
 */

namespace Atomic\LaravelCore;

class HistoryHelper
{

    public static function addHistory(
        $pUser_id, $pTask_id, $pMenu_id, $pBefore_value, 
        $pAfter_value, $pRef_id, $pDesc, $ip_address
    ){
        // Make sure Before and After Value can be encoded to JSON

        $user_id        = $pUser_id;
        $task_id        = $pTask_id;
        $menu_id        = $pMenu_id;
        $before_value   = json_encode($pBefore_value);
        $after_value    = json_encode($pAfter_value);
        $ref_id         = $pRef_id;
        $description    = $pDesc;
        $ip_address     = $ip_address;

        \DB::table('histories')->insert(
            [
                'user_id'       => $user_id,
                'menu_id'       => $menu_id,
                'task_id'       => $task_id,
                'before_value'  => $before_value,
                'after_value'   => $after_value,
                'ref_id'        => $ref_id,
                'description'   => $description,
                'ip_address'    => $ip_address,
                'created_at'    => DateUtil::currentTimestamp(),
                'updated_at'    => DateUtil::currentTimestamp()
            ]
        );
    }

}